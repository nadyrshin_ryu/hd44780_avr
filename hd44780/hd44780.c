//------------------------------------------------------------------------------
// This is Open source software. You can place this code on your site, but don't
// forget a link to my YouTube-channel: https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// ��� ����������� ����������� ���������������� ��������. �� ������ ���������
// ��� �� ����� �����, �� �� �������� ������� ������ �� ��� YouTube-����� 
// "����������� � ���������" https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// �����: �������� ������ / Nadyrshin Ruslan
//------------------------------------------------------------------------------
#include <ioavr.h>
#include <inavr.h>
#include "..\delay\delay.h"
#include "hd44780.h"
#include "cp1251toepson.h"
#include "cp866toepson.h"

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>

#if (HD44780_pcf8574_mode)
  #include "pcf8574.h"
  uint8_t pcf8574_Value = 0;
#endif

// ������� ��� ���������� ��������
#if (HD44780_pcf8574_mode)
  // ������� ��� ���������� ����������� ��� ������ ����� I2C-������� �� pcf8574 
  #define HD44780_RS_HIGH()             {pcf8574_Value |= HD44780_pcf8574_RS_mask; pcf8574_write(HD44780_pcf8574_addr, pcf8574_Value);}
  #define HD44780_RS_LOW()              {pcf8574_Value &= ~HD44780_pcf8574_RS_mask; pcf8574_write(HD44780_pcf8574_addr, pcf8574_Value);}
  #define HD44780_RW_HIGH()             {pcf8574_Value |= HD44780_pcf8574_RW_mask; pcf8574_write(HD44780_pcf8574_addr, pcf8574_Value);}
  #define HD44780_RW_LOW()              {pcf8574_Value &= ~HD44780_pcf8574_RW_mask; pcf8574_write(HD44780_pcf8574_addr, pcf8574_Value);}
  #define HD44780_E_HIGH()              {pcf8574_Value |= HD44780_pcf8574_E_mask; pcf8574_write(HD44780_pcf8574_addr, pcf8574_Value);}
  #define HD44780_E_LOW()               {pcf8574_Value &= ~HD44780_pcf8574_E_mask; pcf8574_write(HD44780_pcf8574_addr, pcf8574_Value);}
  #define HD44780_BL_HIGH()             {pcf8574_Value |= HD44780_pcf8574_BL_mask; pcf8574_write(HD44780_pcf8574_addr, pcf8574_Value);}
  #define HD44780_BL_LOW()              {pcf8574_Value &= ~HD44780_pcf8574_BL_mask; pcf8574_write(HD44780_pcf8574_addr, pcf8574_Value);}
  #define HD44780_SetDATA_4bit(val)     {pcf8574_Value &= ~(0xF << HD44780_Data_Shift); pcf8574_Value |= ((val & 0xF) << HD44780_Data_Shift); pcf8574_write(HD44780_pcf8574_addr, pcf8574_Value);}
  #define HD44780_GetDATA_4bit()        ((pcf8574_read(HD44780_pcf8574_addr) >> HD44780_Data_Shift) & 0xF)
  #define HD44780_SetDATA_PinMode_In(mask)      {pcf8574_Value |= (mask); pcf8574_write(HD44780_pcf8574_addr, pcf8574_Value);}
  #define HD44780_SetDATA_PinMode_Out(mask)     {}
#else
  // ������� ��� ���������� ����������� ��� ������ �������� ����� GPIO ���������������� 
  #define HD44780_RS_HIGH()             HD44780_RS_Port |= HD44780_RS_Mask
  #define HD44780_RS_LOW()              HD44780_RS_Port &= ~HD44780_RS_Mask
  #define HD44780_RW_HIGH()             HD44780_RW_Port |= HD44780_RW_Mask
  #define HD44780_RW_LOW()              HD44780_RW_Port &= ~HD44780_RW_Mask
  #define HD44780_E_HIGH()              HD44780_E_Port |= HD44780_E_Mask
  #define HD44780_E_LOW()               HD44780_E_Port &= ~HD44780_E_Mask
  #define HD44780_BL_HIGH()             HD44780_BL_Port |= HD44780_BL_Mask
  #define HD44780_BL_LOW()              HD44780_BL_Port &= ~HD44780_BL_Mask
  #define HD44780_SetDATA_4bit(val)     {HD44780_Data_Port &= ~(0xF << HD44780_Data_Shift); HD44780_Data_Port |= (val << HD44780_Data_Shift);}
  #define HD44780_SetDATA_8bit(val)     HD44780_Data_Port = val
  #define HD44780_GetDATA_4bit()        ((HD44780_Data_Pin >> HD44780_Data_Shift) & 0xF)
  #define HD44780_GetDATA_8bit()        HD44780_Data_Pin
  #define HD44780_SetDATA_PinMode_In(mask)      HD44780_Data_DDR &= ~mask
  #define HD44780_SetDATA_PinMode_Out(mask)     HD44780_Data_DDR |= mask
#endif

char LastRow = 0;                                               // ��������� ������������� ������
char hd44780_StrBuff[HD44780_COLS * HD44780_ROWS + 8];          // ����� ������ ��� ������



//==============================================================================
// ������� ������ ����� �� hd44780
// - IsCmd - ������ ���������� ����� (����� ����� ������)
//==============================================================================
unsigned char hd44780_read(char IsCmd)
{
  unsigned char Data = 0;
  
  if (IsCmd)
  {
    HD44780_RS_LOW();
  }
  else
  {
    HD44780_RS_HIGH();
  }
  
  HD44780_RW_HIGH();
  
#if HD44780_4bitMode
  // ��������� ������� �������
  HD44780_E_HIGH();
  delay_us(HD44780_ShortDelayUs);
  Data = HD44780_GetDATA_4bit() << 4;
  HD44780_E_LOW();

  delay_us(HD44780_ShortDelayUs);

  // ��������� ������� �������
  HD44780_E_HIGH();
  delay_us(HD44780_ShortDelayUs);
  Data |= HD44780_GetDATA_4bit();
  HD44780_E_LOW();
#else
  HD44780_E_HIGH();
  delay_us(HD44780_ShortDelayMks);
  // ��������� ���� � ���� ������
  Data = HD44780_GetDATA_8bit();
  HD44780_E_LOW();
#endif
  
  HD44780_RW_LOW();

  return Data;
}
//==============================================================================


//==============================================================================
// ��������� ������ ����� �� hd44780
// - IsCmd - ������ ���������� ����� (����� ����� ������)
//==============================================================================
void hd44780_write(unsigned char Data, char IsCmd)
{
  if (IsCmd)
  {
    HD44780_RS_LOW();
  }
  else
  {
    HD44780_RS_HIGH();
  }
  
#if HD44780_4bitMode
  // ����������� ���� ������ ��� ������
  HD44780_SetDATA_PinMode_Out(0xF << HD44780_Data_Shift);
  
  // ����� ������� �������
  HD44780_SetDATA_4bit(Data >> 4);
  HD44780_E_HIGH();
  delay_us(HD44780_ShortDelayUs);
  HD44780_E_LOW();

  delay_us(HD44780_ShortDelayUs);

  // ����� ������� �������
  HD44780_SetDATA_4bit(Data & 0xF);
  HD44780_E_HIGH();
  delay_us(HD44780_ShortDelayUs);
  HD44780_E_LOW();
  
  // ����������� ���� ������ ��� �����
  HD44780_SetDATA_PinMode_In(0xF << HD44780_Data_Shift);
#else
  // ����������� ���� ������ ��� ������
  HD44780_SetDATA_PinMode_Out(0xFF);
  
  // ����� ����
  HD44780_SetDATA_8bit(Data);
  HD44780_E_HIGH();
  delay_us(HD44780_ShortDelayUs);
  HD44780_E_LOW();
  
  // ����������� ���� ������ ��� �����
  HD44780_SetDATA_PinMode_In(0xFF);
#endif
}
//==============================================================================


//==============================================================================
// ������� �������� ������������ ����������� ������� (���� HD44780_WaitBisyFlag=1)
//==============================================================================
#if (HD44780_WaitBisyFlag)
unsigned char hd44780_waitbisy(unsigned char tick)
{
  while ((hd44780_read(1) & 0x80) && (tick)) 
  {
    tick--;
  }
  
  return (tick) ? 0 : 1;
}
#endif
//==============================================================================


//==============================================================================
// ��������� ������ ������� � ��������� ������������ ����������� �������
//==============================================================================
void hd44780_write_cmd(unsigned char Data)
{
#if (HD44780_WaitBisyFlag)
  hd44780_waitbisy(100);
#endif
  
  hd44780_write(Data, 1);

#if (!HD44780_WaitBisyFlag)
  delay_us(HD44780_BisyDelayUs);
#endif
}
//==============================================================================


//==============================================================================
// ��������� ������ ����� ������ � ��������� ������������ ����������� �������
//==============================================================================
void hd44780_write_data(unsigned char Data)
{
#if (HD44780_WaitBisyFlag)
  hd44780_waitbisy(100);
#endif

  hd44780_write(Data, 0);

#if (!HD44780_WaitBisyFlag)
  delay_us(HD44780_BisyDelayUs);
#endif
}
//==============================================================================


//==============================================================================
// ��������� ���������� ���������� ������� (����� I2C-������� ��� ����� ����� GPIO)
//==============================================================================
void hd44780_backlight_set(uint8_t val)
{
  if (val)
  {
    HD44780_BL_HIGH();
  }
  else
  {
    HD44780_BL_LOW();
  }
}
//==============================================================================


//==============================================================================
// ��������� ������������� ��� �� ��� ������ � hd44780
//==============================================================================
void hd44780_bus_init(void)
{
#if (HD44780_pcf8574_mode)
  pcf8574_bus_init();
#else
  // ��� ����, ����������� ����� ����������� ��� ������
  HD44780_E_DDR |= HD44780_E_Mask;
  HD44780_RS_DDR |= HD44780_RS_Mask;

#if (HD44780_BackLightCtrl)
  // ������ ���������� ����������
  HD44780_BL_DDR |= HD44780_BL_Mask;
#endif

#if (HD44780_WaitBisyFlag)
  HD44780_RW_DDR |= HD44780_RW_Mask;
#else
  //HD44780_RW_DDR |= HD44780_RW_Mask;
  //HD44780_RW_LOW();
#endif
  
#endif
}
//==============================================================================


//==============================================================================
// ��������� ������������� ������� (�������� ������������������ ������ � �������)
//==============================================================================
void hd44780_start(void)
{
#if HD44780_4bitMode
  hd44780_write_cmd(0x02);       // ������ �������, ������ ���� ������
  uint8_t Reg = 0x20;
#else
  uint8_t Reg = 0x30;
#endif

#if (HD44780_ROWS > 1)
  Reg |= 0x08;
#endif
  
  hd44780_write_cmd(Reg);       // ������ �������, ������ ���� ������
  delay_ms(10);
  hd44780_write_cmd(0x0C);      // �������� �������
  delay_ms(10);
  hd44780_write_cmd(0x06);      // ������������� ������
  hd44780_clear();              // ������� ������
}
//==============================================================================


//==============================================================================
// ��������� ������������� �������
//==============================================================================
void hd44780_init(void)
{
  hd44780_bus_init();
  delay_ms(100);
  hd44780_start();
}
//==============================================================================


//==============================================================================
// ��������� ��������� ������� �������
//==============================================================================
void hd44780_goto_xy(uint8_t Row, uint8_t Col)
{
  // ��������� ��������� ����� ������ ������
  uint8_t Adr = 0;            
  if (Row & 1)                  // �������� ������ � �.�. hd44780 - ������ ������
    Adr = 0x40;                 // ������ �������� �� ������ ������ hd44780
  if (Row > 1)                  // ��� 4-��������� ������� ��� ������ ������ > 2
    Adr += HD44780_COLS;        // ������ ����� ������ �� ����� ������

  // ��������� ����� � ������
  Adr += Col;
  
  // ����� ���������� ����� DRAM � hd44780
  hd44780_write_cmd(Adr | 0x80);        // ������� ��������� ������ � DRAM
  
  LastRow = Row;
}
//==============================================================================


//==============================================================================
// ��������� ������� �������
//==============================================================================
void hd44780_clear(void)
{
  hd44780_write_cmd(0x01);      // ������� ������

#if (!HD44780_WaitBisyFlag)
  delay_ms(2);
#endif
}
//==============================================================================


//==============================================================================
// ��������� ������ � ������� ������� ����
//==============================================================================
void hd44780_write_buff(char *pBuff, uint8_t Len)
{
  while (Len--)
  {
    hd44780_write_data(*(pBuff++));
  }
}
//==============================================================================


//==============================================================================
// ��������� �������� ANSI-������ � ������� ������� �������
//==============================================================================
void hd44780_puts(char *str)
{
  char i;
  
  while (*str != '\0')
  {
    switch (*str)
    {
    case '\n':  // ������� �� ����� ������
      LastRow++;
      hd44780_goto_xy(LastRow, 0);
      break;
    case '\r':
      hd44780_goto_xy(LastRow, 0);
      break;
    case '\t':
      for (i = 0; i < 4; i++)
        hd44780_write_data(0x20);
      break;
    default:
        hd44780_write_data(*str);
      break;
    }
    str++;
  }
}
//==============================================================================


//==============================================================================
// ��������� ���������������� ������ ������� � ������� ������� �������
//==============================================================================
void hd44780_printf(const char *args, ...)
{
  va_list ap;
  va_start(ap, args);
  char len = vsnprintf(hd44780_StrBuff, sizeof(hd44780_StrBuff), args, ap);
  va_end(ap);

  // ��������������� ������� �������� � ��������� EPSON ��� �������� hd44780
#if (SOURCE_CODEPAGE == CP1251)
  cp1251_to_epson_convert(hd44780_StrBuff);
#else
  cp866_to_epson_convert(hd44780_StrBuff);
#endif
  
  hd44780_puts(hd44780_StrBuff);
}
//==============================================================================
